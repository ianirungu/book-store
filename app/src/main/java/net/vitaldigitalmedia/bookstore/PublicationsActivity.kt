package net.vitaldigitalmedia.bookstore

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class PublicationsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_publications)
    }
}
